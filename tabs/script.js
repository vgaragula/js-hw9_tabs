
let ul = document.querySelector(".tabs");
ul.addEventListener("click", function (ev) {
  let data = ev.target.dataset.type;

  document.querySelector(".active-p").classList.remove("active-p");

  
  document.querySelector(".active-tab").classList.remove("active-tab");

  document.querySelector(`[data-li = ${data}]`).classList.add("active-p");
  ev.target.classList.add("active-tab");
});
